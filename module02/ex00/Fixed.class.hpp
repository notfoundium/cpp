#ifndef FIXED_CLASS_HPP
# define FIXED_CLASS_HPP

class	Fixed {
	public:
		Fixed(void);
		Fixed(Fixed const &fixed);
		~Fixed(void);

		Fixed	&operator=(Fixed const &fixed);

		int		getRawBits(void) const;

		void	setRawBits(int const raw);

	private:
		int					_value;
		static const int	_fb = 8;
};

#endif /* FIXED_CLASS_HPP */
