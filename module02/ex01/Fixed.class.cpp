#include <iostream>
#include <cmath>

#include "Fixed.class.hpp"

Fixed::Fixed(void) {
	std::cout << "Default constructor called" << std::endl;
	this->_value = 0;
	return ;
}

Fixed::Fixed(Fixed const &fixed) {
	std::cout << "Copy constructor called" << std::endl;
	*this = fixed;
	return ;
}

Fixed::Fixed(int value) {
	std::cout << "Int constructor called" << std::endl;
	this->_value = (unsigned int)value << 8;
	return ;
}

Fixed::Fixed(float value) {
	std::cout << "Float constructor called" << std::endl;
	this->_value = 0;
	this->_value = (unsigned int)(value);
	this->_value = this->_value << 8;
	this->_value |= ((unsigned int)roundf(value * 128) & 255);
	return ;
}

Fixed::~Fixed(void) {
	std::cout << "Destructor called" << std::endl;
	return ;
}

Fixed	&Fixed::operator=(Fixed const &fixed) {
	std::cout << "Assignation operator called" << std::endl;
	this->_value = fixed.getRawBits();
	return (*this);
}

std::ostream	&operator<<(std::ostream &o, Fixed const &fixed) {
	o << fixed.toFloat();
	return (o);
}

int		Fixed::toInt(void) const {
	return (this->_value >> 8);
}

float	Fixed::toFloat(void) const {
	unsigned int	value;
	unsigned int	frac = 1;
	unsigned int	appendix = 0;
	float			result = 0;

	value = this->_value;
	for (int i = 7; i >= 0; --i) {
		appendix *= 10;
		if (value & (1 << i)) {
			appendix += frac;
		}
		frac *= 5;
	}
	result = (float)this->toInt() + (float)appendix / 10000000.0f;
	return (result);
}

int		Fixed::getRawBits(void) const {
	return (this->_value);
}

void	Fixed::setRawBits(int const raw) {
	this->_value = raw;
	return ;
}
