#ifndef FIXED_CLASS_HPP
# define FIXED_CLASS_HPP

# include <ostream>

class	Fixed {
	public:
		Fixed(void);
		Fixed(Fixed const &fixed);
		Fixed(int value);
		Fixed(float value);
		~Fixed(void);

		Fixed			&operator=(Fixed const &fixed);

		float	toFloat(void) const;
		int		toInt(void) const;

		int		getRawBits(void) const;

		void	setRawBits(int const raw);

	private:
		int					_value;
		static const int	_fb = 8;
};

std::ostream	&operator<<(std::ostream &o, Fixed const &fixed);

#endif /* FIXED_CLASS_HPP */
