#include <iostream>
#include <iomanip>
#include <string>

#include "Contact.class.hpp"
#include "Phonebook.class.hpp"

bool	execute_command(Phonebook &phonebook, std::string command) {
	if (command == "ADD")
		return (phonebook.add());
	if (command == "SEARCH")
		return (phonebook.search());
	return (false);
}

int		main(void) {
	Phonebook	phonebook;
	std::string	command;

	while (command != "EXIT") {
		std::cout << "phonebook: ";
		std::cin >> command;
		execute_command(phonebook, command);
	}
	return (0);
}
