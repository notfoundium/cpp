#ifndef PHONEBOOK_CLASS_HPP
# define PHONEBOOK_CLASS_HPP

# include "Contact.class.hpp"

class Phonebook {
	public:
		Phonebook(void);
		~Phonebook(void);

		bool	add(void);
		bool	search(void) const;

	private:
		Contact 		_contacts[8];
		unsigned int	_contacts_count;
};

#endif /* PHONEBOOK_CLASS_HPP */