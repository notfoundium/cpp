#include <iostream>
#include <iomanip>
#include <string>

#include "Phonebook.class.hpp"

static std::string	truncate(std::string str) {
	if (str.length() <= 10)
		return (str);
	str = str.substr(0, 10);
	str[9] = '.';
	return (str);
}

Phonebook::Phonebook(void) {
	this->_contacts_count = 0;
	return ;
}

Phonebook::~Phonebook(void) {
	return ;
}

bool	Phonebook::add(void) {
	std::string	input;

	if (this->_contacts_count >= 8) {
		std::cout << "Too many contacts!" << std::endl;
		return (false);
	}
	std::cout << "Input contact first name: ";
	std::cin >> input;
	this->_contacts[_contacts_count].setFirstName(input);
	std::cout << "Input contact last name: ";
	std::cin >> input;
	this->_contacts[_contacts_count].setLastName(input);
	std::cout << "Input contact nickname: ";
	std::cin >> input;
	this->_contacts[_contacts_count].setNickname(input);
	std::cout << "Input contact login: ";
	std::cin >> input;
	this->_contacts[_contacts_count].setLogin(input);
	std::cout << "Input contact postal address: ";
	std::cin >> input;
	this->_contacts[_contacts_count].setPostalAddress(input);
	std::cout << "Input contact email address: ";
	std::cin >> input;
	this->_contacts[_contacts_count].setEmailAddress(input);
	std::cout << "Input contact phone number: ";
	std::cin >> input;
	this->_contacts[_contacts_count].setPhoneNumber(input);
	std::cout << "Input contact birthday date: ";
	std::cin >> input;
	this->_contacts[_contacts_count].setBirtdayDate(input);
	std::cout << "Input contact favorite meal: ";
	std::cin >> input;
	this->_contacts[_contacts_count].setFavoriteMeal(input);
	std::cout << "Input contact underwear color: ";
	std::cin >> input;
	this->_contacts[_contacts_count].setUnderwearColor(input);
	std::cout << "Input contact darkest secret: ";
	std::cin >> input;
	this->_contacts[_contacts_count].setDarkestSecret(input);
	this->_contacts_count++;
	return (true);
}

bool	Phonebook::search(void) const {
	unsigned int	index;

	for (unsigned int i = 0; i < this->_contacts_count; ++i) {
		std::cout << std::setw(10) << i;
		std::cout << "|";
		std::cout << std::setw(10) <<
		truncate(this->_contacts[i].getFirstName());
		std::cout << "|";
		std::cout << std::setw(10) <<
		truncate(this->_contacts[i].getLastName());
		std::cout << "|";
		std::cout << std::setw(10) <<
		truncate(this->_contacts[i].getNickname());
		std::cout << std::endl;
	}
	if (this->_contacts_count > 0) {
		std::cout << "Input contact index: ";
		std::cin >> index;
		while (index > this->_contacts_count - 1) {
			std::cout << "Please input correct index" << std::endl;
			std::cin >> index;
		}
		this->_contacts[index].printInfo();
		return (true);
	}
	return (false);
}