#include <iostream>

#include "Contact.class.hpp"

Contact::Contact(void) {
	return ;
}

Contact::~Contact(void) {
	return ;
}

void		Contact::printInfo(void) const {
	std::cout << this->_first_name << std::endl;
	std::cout << this->_last_name << std::endl;
	std::cout << this->_nickanme << std::endl;
	std::cout << this->_login << std::endl;
	std::cout << this->_postal_address << std::endl;
	std::cout << this->_email_address << std::endl;
	std::cout << this->_phone_number << std::endl;
	std::cout << this->_birthday_date << std::endl;
	std::cout << this->_favorite_meal << std::endl;
	std::cout << this->_underwear_color << std::endl;
	std::cout << this->_darkest_secret << std::endl;
	return ;
}

bool 		Contact::setFirstName(std::string first_name_str) {
	this->_first_name = first_name_str;
	return (true);
}

bool 		Contact::setLastName(std::string last_name_str) {
	this->_last_name = last_name_str;
	return (true);
}

bool 		Contact::setNickname(std::string nickname_str) {
	this->_nickanme = nickname_str;
	return (true);
}

bool 		Contact::setLogin(std::string login_str) {
	this->_login = login_str;
	return (true);
}

bool 		Contact::setPostalAddress(std::string postal_str) {
	this->_postal_address = postal_str;
	return (true);
}

bool 		Contact::setEmailAddress(std::string email_str) {
	this->_email_address = email_str;
	return (true);
}

bool 		Contact::setPhoneNumber(std::string phone_str) {
	this->_phone_number = phone_str;
	return (true);
}

bool 		Contact::setBirtdayDate(std::string birthday_str) {
	this->_birthday_date = birthday_str;
	return (true);
}

bool 		Contact::setFavoriteMeal(std::string meal_str) {
	this->_favorite_meal = meal_str;
	return (true);
}

bool 		Contact::setUnderwearColor(std::string color_str) {
	this->_underwear_color = color_str;
	return (true);
}

bool 		Contact::setDarkestSecret(std::string secret_str) {
	this->_darkest_secret = secret_str;
	return (true);
}

std::string	Contact::getFirstName(void) const {
	return (this->_first_name);
}

std::string	Contact::getLastName(void) const {
	return (this->_last_name);
}

std::string	Contact::getNickname(void) const {
	return (this->_nickanme);
}

std::string	Contact::getLogin(void) const {
	return (this->_login);
}

std::string	Contact::getPostalAddress(void) const {
	return (this->_postal_address);
}

std::string	Contact::getEmailAddress(void) const {
	return (this->_email_address);
}

std::string	Contact::getPhoneNumber(void) const {
	return (this->_phone_number);
}

std::string	Contact::getBirthdayDate(void) const {
	return (this->_birthday_date);
}

std::string	Contact::getFavoriteMeal(void) const {
	return (this->_favorite_meal);
}

std::string	Contact::getUnderwearColor(void) const {
	return (this->_underwear_color);
}

std::string	Contact::getDarkestSecret(void) const {
	return (this->_darkest_secret);
}
