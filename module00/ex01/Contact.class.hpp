#ifndef CONTACT_CLASS_HPP
# define CONTACT_CLASS_HPP

# include <string>

class	Contact {
	public:
		Contact(void);
		~Contact(void);

		void		printInfo(void) const;
		
		bool		setFirstName(std::string first_name_str);
		bool		setLastName(std::string last_name_str);
		bool		setNickname(std::string nickname_str);
		bool		setLogin(std::string login_str);
		bool		setPostalAddress(std::string postal_str);
		bool		setEmailAddress(std::string email_str);
		bool		setPhoneNumber(std::string phone_str);
		bool		setBirtdayDate(std::string birthday_str);
		bool		setFavoriteMeal(std::string meal_str);
		bool		setUnderwearColor(std::string color_str);
		bool		setDarkestSecret(std::string secret_str);

		std::string	getFirstName(void) const;
		std::string	getLastName(void) const;
		std::string	getNickname(void) const;
		std::string	getLogin(void) const;
		std::string	getPostalAddress(void) const;
		std::string	getEmailAddress(void) const;
		std::string	getPhoneNumber(void) const;
		std::string	getBirthdayDate(void) const;
		std::string	getFavoriteMeal(void) const;
		std::string	getUnderwearColor(void) const;
		std::string	getDarkestSecret(void) const;

	private:
		std::string	_first_name;
		std::string	_last_name;
		std::string	_nickanme;
		std::string	_login;
		std::string	_postal_address;
		std::string	_email_address;
		std::string	_phone_number;
		std::string	_birthday_date;
		std::string	_favorite_meal;
		std::string	_underwear_color;
		std::string	_darkest_secret;
};

#endif /* CONTACT_CLASS_HPP */
