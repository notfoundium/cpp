#include <fstream>
#include <iostream>
#include <string>
#include <cstdlib>

void	line_replace(std::string &line,
					std::string str_to_replace,
					std::string pattern) {
	size_t	i;

	i = 0;
	while (true) {
		i = line.find(str_to_replace, i);
		if (i >= line.length())
			break ;
		line.replace(i, str_to_replace.length(), pattern);
		i += pattern.length();
	}
}

void	replace(std::string filename,
				std::string str_to_replace,
				std::string pattern) {
	std::ifstream	ifs;
	std::ofstream	ofs;
	std::string		line;

	ifs.open(filename.c_str());
	if (!ifs.is_open()) {
		std::cerr << "Unable to read the file" << std::endl;
		exit(2);
	}
	ofs.open((filename.append(".replace")).c_str());
	if (!ofs.is_open()) {
		std::cerr << "Unable to write to file" << std::endl;
		exit(2);
	}
	std::getline(ifs, line);
	while (ifs) {
		line_replace(line, str_to_replace, pattern);
		ofs << line << std::endl;
		std::getline(ifs, line);
	}
	ifs.close();
	ofs.close();
}

int		main(int argc, char *argv[]) {
	if (argc != 4) {
		std::cerr << "Invalid arguments!" << std::endl;
		return (1);
	}
	if (!argv[1][0] || !argv[2][0] || !argv[3][0]) {
		std::cerr << "Empty argument!" << std::endl;
		return (1);
	}
	replace(argv[1], argv[2], argv[3]);
	return (0);
}
