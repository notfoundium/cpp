#ifndef ZOMBIE_HPP
# define ZOMBIE_HPP

# include <string>

/*

Compilation string:

clang++ -Wall -Wextra -Werror *.cpp -O0 -g -fsanitize=address

*/

class	Zombie {
	public:
		Zombie(void);
		Zombie(std::string const name, std::string const type);
		~Zombie(void);

		void	announce(void) const;

		void	setName(std::string const name);
		void	setType(std::string const type);

	private:
		std::string	_name;
		std::string	_type;
};

#endif /* ZOMBIE_HPP */
