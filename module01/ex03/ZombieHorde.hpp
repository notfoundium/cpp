#ifndef ZOMBIE_HORDE_HPP
# define ZOMBIE_HORDE_HPP

# include "Zombie.hpp"

class	ZombieHorde {
	public:
		ZombieHorde(int n);
		~ZombieHorde(void);

	private:
		Zombie	*_horde;
};

#endif /* ZOMBIE_HORDE_HPP */
