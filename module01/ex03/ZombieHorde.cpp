#include <string>
#include <sys/time.h>
#include <cstdlib>
#include "ZombieHorde.hpp"

static std::string	randomName(void) {
	std::string rname;
	timeval 	time;
	char 		charset[4] = {'b', 'z', 'p', 'd'};

	gettimeofday(&time, NULL);
	srand(time.tv_usec);
	for (int i = 0; i < 5; i++)
	{
		if (i == 0)
			rname.push_back(toupper(charset[rand() % 4]));
		if (i == 1 || i == 2)
			rname.push_back('o');
		if (i == 3)
		{
			rname.push_back(charset[rand() % 4]);
		}
		if (i == 4)
			rname.push_back('a');
	}
	return (rname);
}

ZombieHorde::ZombieHorde(int n) {
	this->_horde = new Zombie[n];
	for (int i = 0; i < n; ++i) {
		this->_horde[i].setName(randomName());
		this->_horde[i].setType("Default");
		this->_horde[i].announce();
	}
	return ;
}

ZombieHorde::~ZombieHorde(void) {
	delete [] this->_horde;
	return ;
}
