#include "Brain.hpp"
#include <sstream>

static std::string	to_string(unsigned long n) {
	std::ostringstream	ss;

	ss << n;
	return (ss.str());
}

Brain::Brain(void) {
	return ;
}

Brain::~Brain(void) {
	return ;
}

std::string const	Brain::identify(void) {
	std::string	str = to_string((unsigned long)this);
	str.insert(0, "0x");
	return (str);
}
