#ifndef HUMAN_HPP
# define HUMAN_HPP

# include "Brain.hpp"

class	Human {
	public:
		Human();
		~Human();

		std::string const	identify(void);

		Brain				&getBrain(void);

	private:
		Brain	_brain;
};

#endif /* HUMAN_HPP */
