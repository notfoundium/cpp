#ifndef BRAIN_HPP
# define BRAIN_HPP

#include <string>

class	Brain {
	public:
		Brain();
		~Brain();

		std::string const identify(void);
};

#endif /* BRAIN_HPP */
