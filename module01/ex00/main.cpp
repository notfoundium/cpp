#include "Pony.hpp"

void	ponyOnTheHeap(void) {
	Pony	*heapPony = new Pony("The Boss", 10, 100);
	delete heapPony;
	return ;
}

void	ponyOnTheStack(void) {
	Pony	stackPony("Big Boss", 8, 120);
	return ;
}

int		main(void) {
	ponyOnTheHeap();
	ponyOnTheStack();
	return (0);
}
