#include <iostream>
#include "Pony.hpp"

Pony::Pony(std::string name, unsigned int age, double weight) :
					_name(name), _age(age), _weight(weight) {
	std::cout << "New pony was born with name ";
	std::cout << this->_name;
	std::cout << ", age " << this->_age;
	std::cout << " and weight ";
	std::cout << this->_weight << std::endl;
	return ;
}

Pony::~Pony(void) {
	std::cout <<
	"Pony with name " << this->_name << " is dead. :(" <<
	std::endl;
	return ;
}
