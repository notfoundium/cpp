#ifndef PONY_HPP
# define PONY_HPP

# include <string>

/*

Compilation string:

clang++ -std=c++98 -Wall -Wextra -Werror *.cpp

*/

class	Pony {
	public:
		Pony(std::string name, unsigned int age, double weight);
		~Pony(void);

	private:
		std::string		_name;
		unsigned int	_age;
		double			_weight;
};

#endif /* PONY_HPP */
