#include <string>
#include <iostream>

int		main() {
	std::string str = "HI THIS IS BRAIN";
	std::string	*str_p = &str;
	std::string &str_ref = str;

	std::cout << str << std::endl;
	std::cout << *str_p << std::endl;
	std::cout << str_ref << std::endl;
	return (0);
}
