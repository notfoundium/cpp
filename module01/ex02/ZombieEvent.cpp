#include "ZombieEvent.hpp"

ZombieEvent::ZombieEvent(std::string const type) : _type(type) {
	return ;
}

ZombieEvent::~ZombieEvent() {
	return ;
}

void	ZombieEvent::setZombieType(std::string const type) {
	this->_type = type;
	return ;
}

Zombie	*ZombieEvent::newZombie(std::string const name) {
	Zombie	*zombie = new Zombie(name, this->_type);
	return (zombie);
}
