#ifndef ZOMBIE_EVENT_HPP
# define ZOMBIE_EVENT_HPP

# include <string>
# include "Zombie.hpp"

/*

Compilation string:

clang++ -std=c++98 -Wall -Wextra -Werror *.cpp

*/

class	ZombieEvent {
	public:
		ZombieEvent(std::string const type);
		~ZombieEvent(void);

		void	setZombieType(std::string const type);
		Zombie	*newZombie(std::string const name);

	private:
		std::string	_type;
};

#endif /* ZOMBIE_EVENT_HPP */
