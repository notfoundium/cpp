#ifndef ZOMBIE_HPP
# define ZOMBIE_HPP

# include <string>

class	Zombie {
	public:
		Zombie(std::string const name, std::string const type);
		~Zombie(void);

		void	announce(void) const;

		void	setType(std::string const type);

	private:
		std::string	_name;
		std::string	_type;
};

#endif /* ZOMBIE_HPP */
