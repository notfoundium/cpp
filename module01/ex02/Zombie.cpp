#include "Zombie.hpp"

#include <iostream>

Zombie::Zombie(std::string const name, std::string const type) :
				_name(name), _type(type) {
	std::cout << "New zombie with name ";
	std::cout << this->_name;
	std::cout << " and type ";
	std::cout << this->_type << std::endl;
	return ;
}

Zombie::~Zombie(void) {
	std::cout << "Zombie ";
	std::cout << this->_name;
	std::cout << " was slain" << std::endl;
	return ;
}

void	Zombie::announce(void) const {
	std::cout << "<";
	std::cout << this->_name << " ";
	std::cout << "(" << this->_type << ")";
	std::cout << "> Braiiiiiiinnnssss..." << std::endl;
	return ;
}

void	Zombie::setType(std::string const type) {
	this->_type = type;
	return ;
}
