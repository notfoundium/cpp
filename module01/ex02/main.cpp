#include <sys/time.h>
#include <cstdlib>
#include "ZombieEvent.hpp"

void	randomChump() {
	std::string	rname;
	timeval		time;
	char		charset[4] = {'b', 'z', 'p', 'd'};

	gettimeofday(&time, NULL);
	srand(time.tv_usec);
	for (int i = 0; i < 5; i++) {
		if (i == 0)
			rname.push_back(toupper(charset[rand() % 4]));
		if (i == 1 || i == 2)
			rname.push_back('o');
		if (i == 3) {
			rname.push_back(charset[rand() % 4]);
		}
		if (i == 4)
			rname.push_back('a');
	}
	Zombie	zomb(rname, "Default");
	zomb.announce();
	return ;
}

int	main() {
	Zombie		*bigZombie;
	ZombieEvent	event("Big Zombie");

	bigZombie = event.newZombie("BOSS OF THIS GYM");
	bigZombie->announce();
	randomChump();
	randomChump();
	randomChump();
	delete bigZombie;
	return (0);
}
