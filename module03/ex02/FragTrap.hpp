#ifndef FRAGTRAP_CLASS_HPP
# define FRAGTRAP_CLASS_HPP

# include <string>

# include "ClapTrap.hpp"

class	FragTrap : public ClapTrap {
	public:
		FragTrap(std::string const &name);
		~FragTrap(void);

		unsigned int	danceAttack(std::string const &target);
		unsigned int	grenadeAttack(std::string const &target);
		unsigned int	nukeAttack(std::string const &target);
		unsigned int	vaulthunter_dot_exe(std::string const &target);

	private:
		unsigned int	_dance_damage;
		unsigned int	_grenade_damage;
		unsigned int	_nuke_damage;
};

#endif /* FRAGTRAP_CLASS_HPP */
