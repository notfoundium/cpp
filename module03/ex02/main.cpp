#include "FragTrap.hpp"
#include "ScavTrap.hpp"

int		main() {
	FragTrap	poopa("Poopa");
	ScavTrap	loopa("Loopa");

	loopa.takeDamage(poopa.vaulthunter_dot_exe("Loopa"));
	loopa.challengeNewcomer("Poopa");
	poopa.beRepaired(50);
	loopa.beRepaired(50);
	return (0);
}
