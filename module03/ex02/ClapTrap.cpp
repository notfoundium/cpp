#include "ClapTrap.hpp"

#include <iostream>

ClapTrap::ClapTrap(std::string const &name) : _name(name) {
	std::cout << "CL4P-TP " << _name << " created" << std::endl;
	_level = 1;
	return ;
}

ClapTrap::~ClapTrap(void) {
	std::cout << "CL4P-TP " << _name << " created" << std::endl;
	return ;
}

unsigned int	ClapTrap::rangedAttack(std::string const &target) {
	std::cout << "CL4P-TP ";
	std::cout << _name;
	std::cout << " attacks ";
	std::cout << target;
	std::cout << " at range, causing ";
	std::cout << _ranged_damage;
	std::cout << " points of damage!" << std::endl;
	return (_ranged_damage);
}

unsigned int	ClapTrap::meleeAttack(std::string const &target) {
	std::cout << "CL4P-TP ";
	std::cout << _name;
	std::cout << " attacks ";
	std::cout << target;
	std::cout << " in close combat, causing ";
	std::cout << _melee_damage;
	std::cout << " points of damage!" << std::endl;
	return (_melee_damage);
}

void		ClapTrap::takeDamage(unsigned int amount) {
	unsigned int	total;

	if (amount <= _armor)
		total = 0;
	else if (amount - _armor < _hit_points) {
		total = amount - _armor;
		_hit_points -= total;
	}
	else {
		total = amount - _armor;
		_hit_points = 0;
	}
	std::cout << "CL4P-TP ";
	std::cout << _name;
	std::cout << " has taken ";
	std::cout << total;
	std::cout << " damage points. Current HP is: ";
	std::cout << _hit_points << std::endl;
	return ;
}

void		ClapTrap::beRepaired(unsigned int amount) {
	if (_hit_points + amount >= _max_hit_points)
		_hit_points = _max_hit_points;
	else
		_hit_points += amount;
	std::cout << "CL4P-TP ";
	std::cout << _name;
	std::cout << " was repaired in ";
	std::cout << amount;
	std::cout << " hit points. Current HP is: ";
	std::cout << _hit_points << std::endl;
	return ;
}

void	ClapTrap::setMaxHitPoints(unsigned int max_hit_points) {
	this->_max_hit_points = max_hit_points;
	this->_hit_points = max_hit_points;
	return ;
}

void	ClapTrap::setEnergyPoints(unsigned int energy_points) {
	this->_energy_points = energy_points;
	return ;
}

void	ClapTrap::setMaxEnergyPoints(unsigned int max_energy_points) {
	this->_max_energy_points = max_energy_points;
	this->_energy_points = _max_energy_points;
	return ;
}

void	ClapTrap::setMeleeDamage(unsigned int melee_damage) {
	this->_melee_damage = melee_damage;
	return ;
}

void	ClapTrap::setRangedDamage(unsigned int ranged_damage) {
	this->_ranged_damage = ranged_damage;
	return ;
}

void	ClapTrap::setArmor(unsigned int armor) {
	this->_armor = armor;
	return ;
}

unsigned int	ClapTrap::getEnergyPoints(void) const {
	return (this->_energy_points);
}

std::string const	&ClapTrap::getName(void) const {
	return (_name);
}
