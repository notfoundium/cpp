#include "NinjaTrap.hpp"

#include <iostream>

NinjaTrap::NinjaTrap(std::string const& name) : ClapTrap(name) {
	std::cout << "NINJ4-TP " << this->getName() << " created" << std::endl;
	this->setMaxHitPoints(60);
	this->setMaxEnergyPoints(120);
	this->setMeleeDamage(60);
	this->setRangedDamage(5);
	this->setArmor(0);
	_club_damage = 1;
	_katana_damage = 90;
	return ;
}

NinjaTrap::~NinjaTrap(void) {
	std::cout << "NINJ4-TP " << this->getName() << " destroyed";std::cout << std::endl;
	return ;
}

void	NinjaTrap::ninjaShoebox(ClapTrap& clapTrap) {
	if (this->getEnergyPoints() < 20) {
		std::cout << "NINJ4-TP ";
		std::cout << this->getName();
		std::cout << " out of energy..." << std::endl;
		return ;
	}
	this->setEnergyPoints(this->getEnergyPoints() - 20);
	std::cout << "Bonk!" << std::endl;
	clapTrap.takeDamage(_club_damage);
	return ;
}

void	NinjaTrap::ninjaShoebox(NinjaTrap& ninjaTrap) {
	if (this->getEnergyPoints() < 20) {
		std::cout << "NINJ4-TP ";
		std::cout << this->getName();
		std::cout << " out of energy..." << std::endl;
		return ;
	}
	this->setEnergyPoints(this->getEnergyPoints() - 20);
	std::cout << "NINJ4-TP ";
	std::cout << this->getName();
	std::cout << " throws shuriken" << std::endl;
	ninjaTrap.takeDamage(this->getRangedDamage());
	return ;
}

void	NinjaTrap::ninjaShoebox(FragTrap& fragTrap) {
	if (this->getEnergyPoints() < 20) {
		std::cout << "NINJ4-TP ";
		std::cout << this->getName();
		std::cout << " out of energy..." << std::endl;
		return ;
	}
	this->setEnergyPoints(this->getEnergyPoints() - 20);
	std::cout << "NINJ4-TP ";
	std::cout << this->getName();
	std::cout << " cuts with katana" << std::endl;
	fragTrap.takeDamage(_katana_damage);
	return ;
}

void	NinjaTrap::ninjaShoebox(ScavTrap& scavTrap) {
	if (this->getEnergyPoints() < 20) {
		std::cout << "NINJ4-TP ";
		std::cout << this->getName();
		std::cout << " out of energy..." << std::endl;
		return ;
	}
	this->setEnergyPoints(this->getEnergyPoints() - 20);
	std::cout << "NINJ4-TP ";
	std::cout << this->getName();
	std::cout << " makes I DOOO KEN!" << std::endl;
	scavTrap.takeDamage(this->getMeleeDamage());
	return ;
}
