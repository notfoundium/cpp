#ifndef SUPERTRAP_CLASS_HPP
# define SUPERTRAP_CLASS_HPP

# include "FragTrap.hpp"
# include "NinjaTrap.hpp"

class	SuperTrap : public FragTrap, public NinjaTrap {
	public:
		SuperTrap(std::string const& name);
		~SuperTrap(void);

	private:

};

#endif /* SUPERTRAP_CLASS_HPP */
