#include "FragTrap.hpp"
#include "ScavTrap.hpp"
#include "NinjaTrap.hpp"
#include "SuperTrap.hpp"

int		main() {
	SuperTrap	super("Super");
	FragTrap	frag("Frag");
	NinjaTrap	ninja("Ninja");
	ScavTrap	scav("Scav");

	super.ninjaShoebox(frag);
	super.ninjaShoebox(ninja);
	super.ninjaShoebox(scav);
	frag.beRepaired(100);
	ninja.beRepaired(100);
	scav.beRepaired(100);
	frag.takeDamage(super.vaulthunter_dot_exe("Frag"));
	ninja.takeDamage(super.vaulthunter_dot_exe("Ninja"));
	scav.takeDamage(super.vaulthunter_dot_exe("Scav"));
	return (0);
}
