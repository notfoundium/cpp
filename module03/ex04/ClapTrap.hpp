#ifndef CLAPTRAP_CLASS_HPP
# define CLAPTRAP_CLASS_HPP

#include <string>

class	ClapTrap {
	public:
		ClapTrap(std::string const &name);
		~ClapTrap(void);

		unsigned int	rangedAttack(std::string const &target);
		unsigned int	meleeAttack(std::string const &target);
		void			takeDamage(unsigned int amount);
		void			beRepaired(unsigned int amount);

		void		setHitPoints(unsigned int hit_points);
		void		setMaxHitPoints(unsigned int max_hit_points);
		void		setEnergyPoints(unsigned int energy_points);
		void		setMaxEnergyPoints(unsigned int max_energy_points);
		void		setMeleeDamage(unsigned int melee_damage);
		void		setRangedDamage(unsigned int ranged_damage);
		void		setArmor(unsigned int armor);

		unsigned int		getEnergyPoints(void) const;
		std::string const	&getName(void) const;
		unsigned int		getMeleeDamage(void) const;
		unsigned int		getRangedDamage(void) const;

	private:
		unsigned int	_hit_points;
		unsigned int	_max_hit_points;
		unsigned int	_energy_points;
		unsigned int	_max_energy_points;
		unsigned int	_level;
		std::string		_name;
		unsigned int	_melee_damage;
		unsigned int	_ranged_damage;
		unsigned int	_armor;
};

#endif /* CLAPTRAP_CLASS_HPP */
