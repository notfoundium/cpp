#ifndef NINJATRAP_CLASS_HPP
# define NINJATRAP_CLASS_HPP

# include "ClapTrap.hpp"
# include "FragTrap.hpp"
# include "ScavTrap.hpp"

class	NinjaTrap : public virtual ClapTrap{
	public:
		NinjaTrap(std::string const& name);
		~NinjaTrap(void);

		void	ninjaShoebox(ClapTrap& clapTrap);
		void	ninjaShoebox(NinjaTrap& ningaTrap);
		void	ninjaShoebox(FragTrap& fragTrap);
		void	ninjaShoebox(ScavTrap& scavTrap);

	private:
		unsigned int	_club_damage;
		unsigned int	_katana_damage;
};

#endif /* NINJATRAP_CLASS_HPP */
