#include "SuperTrap.hpp"

#include "iostream"

SuperTrap::SuperTrap(std::string const& name) : ClapTrap(name), FragTrap(name), NinjaTrap(name) {
	std::cout << "SUP3R-TP " << this->getName() << " created" << std::endl;
	this->setMaxHitPoints(100);
	this->setMaxEnergyPoints(120);
	this->setMeleeDamage(60);
	this->setRangedDamage(20);
	this->setArmor(5);
	return ;
}

SuperTrap::~SuperTrap(void) {
	std::cout << "SUP3R-TP " << this->getName() << " destroyed";
	std::cout << std::endl;
	return ;
}
