#include "FragTrap.hpp"
#include "ScavTrap.hpp"
#include "NinjaTrap.hpp"

int		main() {
	FragTrap	frag("Frag");
	ScavTrap	scav("Scav");
	NinjaTrap	ninja("Ninja");
	NinjaTrap	yaNinja("Yet another Ninja");

	ninja.ninjaShoebox(frag);
	ninja.ninjaShoebox(scav);
	ninja.ninjaShoebox(yaNinja);
	return (0);
}
