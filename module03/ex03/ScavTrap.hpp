#ifndef SCAVTRAP_CLASS_HPP
# define SCAVTRAP_CLASS_HPP

# include <string>

# include "ClapTrap.hpp"

class	ScavTrap : public ClapTrap {
	public:
		ScavTrap(std::string const &name);
		~ScavTrap(void);

		void			challengeNewcomer(std::string const &target);

	private:
};

#endif /* SCAVTRAP_CLASS_HPP */
