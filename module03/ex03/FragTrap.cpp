#include "FragTrap.hpp"

#include <iostream>
#include <sys/time.h>

FragTrap::FragTrap(std::string const &name) : ClapTrap(name) {
	std::cout << "FR4G-TP " << this->getName() << " created" << std::endl;
	this->setMaxHitPoints(100);
	this->setMaxEnergyPoints(100);
	this->setMeleeDamage(30);
	this->setRangedDamage(20);
	this->setArmor(5);
	_nuke_damage = 100;
	_grenade_damage = 50;
	_dance_damage = 0;
	return ;
}

FragTrap::~FragTrap(void) {
	std::cout << "FR4G-TP " << this->getName() << " destroyed" << std::endl;
	return ;
}

unsigned int	FragTrap::danceAttack(std::string const &target) {
	std::cout << "FR4G-TP ";
	std::cout << this->getName();
	std::cout << " dancing to death before ";
	std::cout << target;
	std::cout << " and deals ";
	std::cout << _dance_damage;
	std::cout << " points of damage!" << std::endl;
	return (_dance_damage);
}

unsigned int	FragTrap::grenadeAttack(std::string const &target) {
	std::cout << "FR4G-TP ";
	std::cout << this->getName();
	std::cout << " attacks ";
	std::cout << target;
	std::cout << " with grenade, causing ";
	std::cout << _grenade_damage;
	std::cout << " points of damage!" << std::endl;
	return (_grenade_damage);
}

unsigned int	FragTrap::nukeAttack(std::string const &target) {
	std::cout << "OMG! FR4G-TP ";
	std::cout << this->getName();
	std::cout << " attacks ";
	std::cout << target;
	std::cout << " with !!!NUKE!!!, causing ";
	std::cout << _nuke_damage;
	std::cout << " points of damage! That hurts..." << std::endl;
	return (_nuke_damage);
}

unsigned int	FragTrap::vaulthunter_dot_exe(std::string const &target) {
	int				check;
	struct timeval	tv;

	if (this->getEnergyPoints() < 25) {
		std::cout << "Awww Hell... FR4G-TP ";
		std::cout << this->getName();
		std::cout << " runnin' at low of energy!" << std::endl;
		return (0);
	}
	gettimeofday(&tv, NULL);
	this->setEnergyPoints(this->getEnergyPoints() - 25);
	check = tv.tv_usec % 5;
	if (check == 0)
		return (this->meleeAttack(target));
	else if (check == 1)
		return (this->rangedAttack(target));
	else if (check == 2)
		return (this->danceAttack(target));
	else if (check == 3)
		return (this->grenadeAttack(target));
	else
		return (this->nukeAttack(target));
}
