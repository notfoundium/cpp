#include "ScavTrap.hpp"

#include <iostream>
#include <sys/time.h>

ScavTrap::ScavTrap(std::string const &name) : ClapTrap(name) {
	std::cout << "SC4V-TP " << this->getName() << " created" << std::endl;
	this->setMaxHitPoints(100);
	this->setMaxEnergyPoints(50);
	this->setMeleeDamage(20);
	this->setRangedDamage(15);
	this->setArmor(3);
	return ;
}

ScavTrap::~ScavTrap(void) {
	std::cout << "SC4V-TP " << this->getName() << " destroyed" << std::endl;
	return ;
}

void			ScavTrap::challengeNewcomer(std::string const &target) {
	int				check;
	struct timeval	tv;

	if (this->getEnergyPoints() < 25) {
		std::cout << "Awww Hell... SC4V-TP ";
		std::cout << this->getName();
		std::cout << " runnin' at low of energy!" << std::endl;
		return ;
	}
	gettimeofday(&tv, NULL);
	this->setEnergyPoints(this->getEnergyPoints() - 25);
	check = tv.tv_usec % 3;
	std::cout << "SC4V-TP ";
	std::cout << this->getName();
	std::cout << " try to challnge ";
	std::cout << target;
	std::cout << " in ";
	if (check == 0) {
		std::cout << "tag";
	}
	else if (check == 1) {
		std::cout << "hide and seek";
	}
	else {
		std::cout << "drinking";
	}
	std::cout << std::endl;
	return ;
}
