#ifndef FRAGTRAP_CLASS_HPP
# define FRAGTRAP_CLASS_HPP

# include <string>

class	FragTrap {
	public:
		FragTrap(std::string name);
		~FragTrap(void);

		unsigned int	rangedAttack(std::string const &target);
		unsigned int	meleeAttack(std::string const &target);
		unsigned int	danceAttack(std::string const &target);
		unsigned int	grenadeAttack(std::string const &target);
		unsigned int	nukeAttack(std::string const &target);
		void			takeDamage(unsigned int amount);
		void			beRepaired(unsigned int amount);
		unsigned int	vaulthunter_dot_exe(std::string const &target);

	private:
		unsigned int	_hit_points;
		unsigned int	_max_hit_points;
		unsigned int	_energy_points;
		unsigned int	_max_energy_points;
		unsigned int	_level;
		std::string		_name;
		unsigned int	_melee_damage;
		unsigned int	_ranged_damage;
		unsigned int	_dance_damage;
		unsigned int	_grenade_damage;
		unsigned int	_nuke_damage;
		unsigned int	_armor;
};

#endif /* FRAGTRAP_CLASS_HPP */
