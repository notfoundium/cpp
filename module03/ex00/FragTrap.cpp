#include "FragTrap.hpp"

#include <iostream>
#include <sys/time.h>

FragTrap::FragTrap(std::string name) : _name(name) {
	std::cout << "FR4G-TP " << _name << " created" << std::endl;
	_hit_points = 100;
	_max_hit_points = 100;
	_energy_points = 100;
	_max_energy_points = 100;
	_level = 1;
	_melee_damage = 30;
	_ranged_damage = 20;
	_armor = 5;
	_nuke_damage = 100;
	_grenade_damage = 50;
	_dance_damage = 0;
	return ;
}

FragTrap::~FragTrap(void) {
	std::cout << "FR4G-TP " << _name << " destroyed";
	std::cout << std::endl;
	return ;
}

unsigned int	FragTrap::rangedAttack(std::string const &target) {
	std::cout << "FR4G-TP ";
	std::cout << _name;
	std::cout << " attacks ";
	std::cout << target;
	std::cout << " at range, causing ";
	std::cout << _ranged_damage;
	std::cout << " points of damage!" << std::endl;
	return (_ranged_damage);
}

unsigned int	FragTrap::meleeAttack(std::string const &target) {
	std::cout << "FR4G-TP ";
	std::cout << _name;
	std::cout << " attacks ";
	std::cout << target;
	std::cout << " in close combat, causing ";
	std::cout << _melee_damage;
	std::cout << " points of damage!" << std::endl;
	return (_melee_damage);
}

unsigned int	FragTrap::danceAttack(std::string const &target) {
	std::cout << "FR4G-TP ";
	std::cout << _name;
	std::cout << " dancing to death before ";
	std::cout << target;
	std::cout << " and deals ";
	std::cout << _dance_damage;
	std::cout << " points of damage!" << std::endl;
	return (_dance_damage);
}

unsigned int	FragTrap::grenadeAttack(std::string const &target) {
	std::cout << "FR4G-TP ";
	std::cout << _name;
	std::cout << " attacks ";
	std::cout << target;
	std::cout << " with grenade, causing ";
	std::cout << _grenade_damage;
	std::cout << " points of damage!" << std::endl;
	return (_grenade_damage);
}

unsigned int	FragTrap::nukeAttack(std::string const &target) {
	std::cout << "OMG! FR4G-TP ";
	std::cout << _name;
	std::cout << " attacks ";
	std::cout << target;
	std::cout << " with !!!NUKE!!!, causing ";
	std::cout << _nuke_damage;
	std::cout << " points of damage! That hurts..." << std::endl;
	return (_nuke_damage);
}

void		FragTrap::takeDamage(unsigned int amount) {
	unsigned int	total;

	if (amount <= _armor)
		total = 0;
	else if (amount - _armor < _hit_points) {
		total = amount - _armor;
		_hit_points -= total;
	}
	else {
		total = amount - _armor;
		_hit_points = 0;
	}
	std::cout << "FR4G-TP ";
	std::cout << _name;
	std::cout << " has taken ";
	std::cout << total;
	std::cout << " damage points. Current HP is: ";
	std::cout << _hit_points << std::endl;
	return ;
}

void		FragTrap::beRepaired(unsigned int amount) {
	if (_hit_points + amount >= _max_hit_points)
		_hit_points = _max_hit_points;
	else
		_hit_points += amount;
	std::cout << "FR4G-TP ";
	std::cout << _name;
	std::cout << " was repaired in ";
	std::cout << amount;
	std::cout << " hit points. Current HP is: ";
	std::cout << _hit_points << std::endl;
	return ;
}

unsigned int	FragTrap::vaulthunter_dot_exe(std::string const &target) {
	int				check;
	struct timeval	tv;

	if (_energy_points < 25) {
		std::cout << "Awww Hell... I'm runnin' at low of energy!" << std::endl;
		return (0);
	}
	gettimeofday(&tv, NULL);
	_energy_points -= 25;
	check = tv.tv_usec % 5;
	if (check == 0)
		return (this->meleeAttack(target));
	else if (check == 1)
		return (this->rangedAttack(target));
	else if (check == 2)
		return (this->danceAttack(target));
	else if (check == 3)
		return (this->grenadeAttack(target));
	else
		return (this->nukeAttack(target));
}
