#include "FragTrap.hpp"

int		main() {
	FragTrap	poopa("Poopa");
	FragTrap	loopa("Loopa");

	loopa.takeDamage(poopa.vaulthunter_dot_exe("Loopa"));
	poopa.takeDamage(loopa.vaulthunter_dot_exe("Poopa"));
	loopa.beRepaired(95);
	poopa.beRepaired(95);
	loopa.takeDamage(poopa.vaulthunter_dot_exe("Loopa"));
	poopa.takeDamage(loopa.vaulthunter_dot_exe("Poopa"));
	loopa.beRepaired(95);
	poopa.beRepaired(95);
	loopa.takeDamage(poopa.vaulthunter_dot_exe("Loopa"));
	poopa.takeDamage(loopa.vaulthunter_dot_exe("Poopa"));
	loopa.beRepaired(95);
	poopa.beRepaired(95);
	loopa.takeDamage(poopa.vaulthunter_dot_exe("Loopa"));
	poopa.takeDamage(loopa.vaulthunter_dot_exe("Poopa"));
	loopa.beRepaired(95);
	poopa.beRepaired(95);
	loopa.takeDamage(poopa.vaulthunter_dot_exe("Loopa"));
	poopa.takeDamage(loopa.vaulthunter_dot_exe("Poopa"));
	return (0);
}
