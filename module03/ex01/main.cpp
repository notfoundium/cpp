#include "FragTrap.hpp"
#include "ScavTrap.hpp"

int		main() {
	FragTrap	poopa("Poopa");
	ScavTrap	loopa("Loopa");

	loopa.takeDamage(poopa.vaulthunter_dot_exe("Loopa"));
	loopa.challengeNewcomer("Poopa");
	loopa.beRepaired(95);
	poopa.beRepaired(95);
	loopa.takeDamage(poopa.vaulthunter_dot_exe("Loopa"));
	loopa.challengeNewcomer("Poopa");
	loopa.beRepaired(95);
	poopa.beRepaired(95);
	loopa.takeDamage(poopa.vaulthunter_dot_exe("Loopa"));
	loopa.challengeNewcomer("Poopa");
	loopa.beRepaired(95);
	poopa.beRepaired(95);
	loopa.takeDamage(poopa.vaulthunter_dot_exe("Loopa"));
	loopa.challengeNewcomer("Poopa");
	loopa.beRepaired(95);
	poopa.beRepaired(95);
	loopa.takeDamage(poopa.vaulthunter_dot_exe("Loopa"));
	loopa.challengeNewcomer("Poopa");
	return (0);
}
