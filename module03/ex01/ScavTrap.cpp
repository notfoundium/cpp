#include "ScavTrap.hpp"

#include <iostream>
#include <sys/time.h>

ScavTrap::ScavTrap(std::string const &name) : _name(name) {
	std::cout << "SC4V-TP " << _name << " created" << std::endl;
	_hit_points = 100;
	_max_hit_points = 100;
	_energy_points = 50;
	_max_energy_points = 50;
	_level = 1;
	_melee_damage = 20;
	_ranged_damage = 15;
	_armor = 3;
	return ;
}

ScavTrap::~ScavTrap(void) {
	std::cout << "SC4V-TP " << _name << " destroyed" << std::endl;
	return ;
}

unsigned int	ScavTrap::rangedAttack(std::string const &target) {
	std::cout << "SC4V-TP ";
	std::cout << _name;
	std::cout << " attacks ";
	std::cout << target;
	std::cout << " at range, causing ";
	std::cout << _ranged_damage;
	std::cout << " points of damage!" << std::endl;
	return (_ranged_damage);
}

unsigned int	ScavTrap::meleeAttack(std::string const &target) {
	std::cout << "SC4V-TP ";
	std::cout << _name;
	std::cout << " attacks ";
	std::cout << target;
	std::cout << " in close combat, causing ";
	std::cout << _melee_damage;
	std::cout << " points of damage!" << std::endl;
	return (_melee_damage);
}

void		ScavTrap::takeDamage(unsigned int amount) {
	unsigned int	total;

	if (amount <= _armor)
		total = 0;
	else if (amount - _armor < _hit_points) {
		total = amount - _armor;
		_hit_points -= total;
	}
	else {
		total = amount - _armor;
		_hit_points = 0;
	}
	std::cout << "SC4V-TP ";
	std::cout << _name;
	std::cout << " has taken ";
	std::cout << total;
	std::cout << " damage points. Current HP is: ";
	std::cout << _hit_points << std::endl;
	return ;
}

void		ScavTrap::beRepaired(unsigned int amount) {
	if (_hit_points + amount >= _max_hit_points)
		_hit_points = _max_hit_points;
	else
		_hit_points += amount;
	std::cout << "SC4V-TP ";
	std::cout << _name;
	std::cout << " was repaired in ";
	std::cout << amount;
	std::cout << " hit points. Current HP is: ";
	std::cout << _hit_points << std::endl;
	return ;
}

void			ScavTrap::challengeNewcomer(std::string const &target) {
	int				check;
	struct timeval	tv;

	if (_energy_points < 25) {
		std::cout << "Awww Hell... SC4V-TP ";
		std::cout << _name;
		std::cout << " runnin' at low of energy!" << std::endl;
		return ;
	}
	gettimeofday(&tv, NULL);
	_energy_points -= 25;
	check = tv.tv_usec % 3;
	std::cout << "SC4V-TP ";
	std::cout << _name;
	std::cout << " try to challnge ";
	std::cout << target;
	std::cout << " in ";
	if (check == 0) {
		std::cout << "tag";
	}
	else if (check == 1) {
		std::cout << "hide and seek";
	}
	else {
		std::cout << "drinking";
	}
	std::cout << std::endl;
	return ;
}
